﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.IO;
using SAPbobsCOM;
using System.Xml;
using CompuTec.ProcessForce.API;
using CompuTec.ProcessForce.API.Actions.CreatePickOrderForProductionIssue;

using CompuTec.ProcessForce.API.Documents.PickOrder;
using CompuTec.ProcessForce.API.Documents.ItemDetails;
using CompuTec.ProcessForce.API.Documents.BillOfMaterials;
using CompuTec.ProcessForce.API.Core;
using SAPbobsCOM;
using System.Text.RegularExpressions;

namespace ImportExportViand
{
    class Program
    {

        public static SAPbobsCOM.Company oCompany;
        public static IProcessForceCompany PFcompany;
        public static void Write_To_file(string data)
        {
            try
            {
                string filePath = ConfigurationManager.AppSettings["log"];
                filePath = filePath.Replace(".txt", "_" + DateTime.Now.ToString("ddMMyyy") + ".txt");
                if (!Directory.Exists(filePath))
                {
                    Directory.CreateDirectory(System.IO.Path.GetDirectoryName(filePath));
                }

                if (!File.Exists(filePath))
                {
                    FileStream fs = new FileStream(filePath, FileMode.Create,
                        FileAccess.Write, FileShare.ReadWrite);
                    fs.Close();
                }

                if (File.Exists(filePath))
                {
                    using (FileStream fs = new FileStream(filePath, FileMode.Append,
                        FileAccess.Write, FileShare.ReadWrite))
                    {
                        using (StreamWriter sw = new StreamWriter(fs))
                        {
                            sw.AutoFlush = true;
                            sw.WriteLine(DateTime.Now.ToString() + data);
                        }
                    }
                }
            }
            catch (Exception exp)
            {
                Console.WriteLine(exp.Message);
            }

        }
        public static void WriteErrorTable(string Error, string file, string proces)
        {
            string query = @"INSERT INTO [@IMPORT_ERRORS]
                                select 
	                            (select isnull(max(convert(numeric,code))+1,1) from [@IMPORT_ERRORS]),
                                (select isnull(max(convert(numeric,code))+1,1) from [@IMPORT_ERRORS]),
	                            '{0}',
	                            '{1}',
	                            '{2}',
                                 getdate() ";
            SAPbobsCOM.Recordset oRcc = oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
            try
            {
                oRcc.DoQuery(string.Format(query, file, proces, Error));
            }
            catch(Exception ex)
            {
                Write_To_file(ex.Message);
            }
            

        }
        #region Connection
        private static bool ConnectToSBO()
        {
            bool flag = false;
            try
            {
                flag = false;
                oCompany = new SAPbobsCOM.Company();



                // All settings must be in a configuration file and not static
                switch (ConfigurationManager.AppSettings["dbservertype"])
                {
                    case "2008":
                        oCompany.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2008;
                        break;
                    case "2012":
                        oCompany.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2012;
                        break;
                    case "2014":
                        oCompany.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2014;
                        break;
                    //case "2016":
                    //    oCompany.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2016;
                    //    break;
                    default:
                        oCompany.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2012;
                        break;

                }

                oCompany.DbUserName = EncryptDecrypt.Decrypt(ConfigurationManager.AppSettings["dbusername"]);
                oCompany.DbPassword = EncryptDecrypt.Decrypt(ConfigurationManager.AppSettings["dbpass"]);
                oCompany.CompanyDB = ConfigurationManager.AppSettings["company"];
                oCompany.Server = ConfigurationManager.AppSettings["server"];
                oCompany.UserName = EncryptDecrypt.Decrypt(ConfigurationManager.AppSettings["username"]);
                oCompany.Password = EncryptDecrypt.Decrypt(ConfigurationManager.AppSettings["pass"]);
                /*
                oCompany.DbUserName = EncryptDecrypt.Decrypt(ConfigurationManager.AppSettings["dbusername"]);
                oCompany.DbPassword = EncryptDecrypt.Decrypt(ConfigurationManager.AppSettings["dbpass"]);
                oCompany.CompanyDB = ConfigurationManager.AppSettings["company"];
                oCompany.Server = ConfigurationManager.AppSettings["server"];
                oCompany.UserName = EncryptDecrypt.Decrypt(ConfigurationManager.AppSettings["username"]);
                oCompany.Password = EncryptDecrypt.Decrypt(ConfigurationManager.AppSettings["pass"]);
                */
                if (oCompany.Connect() != 0)
                {
                    int errCode;
                    string errMsg;
                    oCompany.GetLastError(out errCode, out errMsg);
                    Write_To_file(DateTime.Now.ToString() + ": " + errCode + " " + errMsg);
                    //Errors += "- " + errCode + " " + errMsg + "<br/>";
                    return flag;
                }
                flag = true;
                Write_To_file(DateTime.Now.ToString() + ": Successfully Connected to Company!");
                Console.WriteLine(DateTime.Now.ToString() + ": Successfully Connected to Company!");
            }
            catch (Exception exc)
            {
                Write_To_file(DateTime.Now.ToString() + ": " + exc.Message + " " + exc.StackTrace);
                //Errors += "- " + exc.Message + " " + exc.StackTrace + "<br/>";

            }
            return flag;
        }
        private static void ConnectToCompany()
        {
            try
            {
                //CompuTec.Core.Connection.ConnectionHolder.ConType = CompuTec.Core.Connection.ConnectionType.UIDI;
                //var obj=  CompuTec.Core.Connection.ConnectionHolder.UIDIConnection.GetCompany();
                //Application app = null;
                //PFcompany = ProcessForceCompanyInitializator.ConnectUI(out app, false);
                PFcompany = ProcessForceCompanyInitializator.CreateCompany();
                //SAPbobsCOM.Company compSAP = CompuTec.Core.Connection.ConnectionHolder.UIDIConnection.GetCompany();

                #region Connect To Company
                PFcompany.SQLServer = ConfigurationManager.AppSettings["Server"];//compSAP.Server;// "DEV-ORTHO-001";
                PFcompany.UseTrusted = false;
                PFcompany.SQLUserName = EncryptDecrypt.Decrypt(ConfigurationManager.AppSettings["dbusername"]);
                PFcompany.SQLPassword = EncryptDecrypt.Decrypt(ConfigurationManager.AppSettings["dbpass"]);
                PFcompany.UserName = EncryptDecrypt.Decrypt(ConfigurationManager.AppSettings["username"]);
                PFcompany.Password = EncryptDecrypt.Decrypt(ConfigurationManager.AppSettings["pass"]);
               //PFcompany.LicenseServer = ConfigurationManager.AppSettings["LicenseServer"];  //(compSAP.LicenseServer!=String.Empty && compSAP.LicenseServer.IndexOf(":")>=0)? compSAP.LicenseServer.Split(':')[0]: compSAP.LicenseServer;

                switch (ConfigurationManager.AppSettings["dbservertype"])
                {
                    case "2005":
                        PFcompany.DbServerType = BoDataServerTypes.dst_MSSQL2005;
                        break;
                    case "2008":
                        PFcompany.DbServerType = BoDataServerTypes.dst_MSSQL2008;
                        break;
                    case "2012":
                        PFcompany.DbServerType = BoDataServerTypes.dst_MSSQL2012;
                        break;
                    case "2014":
                        PFcompany.DbServerType = BoDataServerTypes.dst_MSSQL2014;
                        break;
                    //case "2016":
                    //    PFcompany.DbServerType = BoDataServerTypes.dst_MSSQL2016;
                    //    break;
                    case "HANA":
                        PFcompany.DbServerType = BoDataServerTypes.dst_HANADB;
                        break;
                    default:
                        PFcompany.DbServerType = BoDataServerTypes.dst_MSSQL2012;
                        break;
                }

                PFcompany.Databasename = ConfigurationManager.AppSettings["company"];
                PFcompany.Language = SAPbobsCOM.BoSuppLangs.ln_English;
                Write_To_file(string.Format("Connecting to PFCompany"));
                if (PFcompany.Connect() == 1)
                {
                    Write_To_file(string.Format("Connected to Company {0}", PFcompany.SapCompany.CompanyName));
                }
                else
                    Write_To_file(string.Format("Not Connected To compmany Error:{0}", PFcompany.getLastErrorDescription()));
            }
            catch (Exception ex)
            {
                Write_To_file(string.Format("Error:{0}", ex.Message + "-" + ex.StackTrace));
            }

            #endregion
        }
        #endregion

        static void Main(string[] args)
        {
            if (ConnectToSBO())
            {
                ConnectToCompany();

                DateTime StartHour = DateTime.Now;
                int TimeForRun = int.Parse(ConfigurationManager.AppSettings["TimeForRun"]);
                int SleepTime = int.Parse(ConfigurationManager.AppSettings["SleepTime"]);
                while (StartHour.AddHours(TimeForRun) > DateTime.Now)
                {
                    Write_To_file(" START ");
                    if (ConfigurationManager.AppSettings["ImportAllix"] == "1")
                    {
                        #region Import Allix


                        if (PFcompany.IsConnected)
                        {
                            string PathImport = ConfigurationManager.AppSettings["ImportPathALLIX"];
                            string PathFail = ConfigurationManager.AppSettings["ImportPathALLIX"] + "KO\\";
                            if (!Directory.Exists(PathFail))
                            {
                                Directory.CreateDirectory(PathFail);
                            }
                            string PathSucces = ConfigurationManager.AppSettings["ImportPathALLIX"] + "OK\\";
                            if (!Directory.Exists(PathSucces))
                            {
                                Directory.CreateDirectory(PathSucces);
                            }
                            int AddID = 0;
                            int AddBom = 0;
                            try
                            {

                            }
                            catch (Exception ex)
                            {
                                Write_To_file(ex.Message);
                            }
                            string[] files = Directory.GetFiles(PathImport);
                            Write_To_file("S-au gasit " + files.Count().ToString() + " fisiere ALLIX  de procesat ");
                            foreach (string file in files)
                            {
                                bool HasErrors = false;
                                string filename = Path.GetFileName(file);
                                Write_To_file(file);
                                string path = file;

                                string recipes = System.IO.File.ReadAllText(path);
                                string pattern = @"0,0,";

                                var result2 = Regex.Split(recipes, pattern);
                                for (int i = 1; i < result2.Count<string>(); i++)
                                {

                                    result2[i] = "0,0," + result2[i];
                                    string[] lines = result2[i].Split(new[] { Environment.NewLine }, StringSplitOptions.None);


                                    #region Recipe 

                                    var Item_LInes = lines.ToList().Where(x => x.StartsWith("1"));
                                    var labelLines = lines.ToList().Where(x => x.StartsWith("4"));
                                    var cevalLines = lines.ToList().Where(x => x.StartsWith("3"));
                                    var HeaderLines = lines.ToList().Where(x => !x.StartsWith("3") && !x.StartsWith("4") && !x.StartsWith("1"));
                                    string[] CodeDet = (HeaderLines.ToArray())[0].Split(',');
                                    string[] Formula = (HeaderLines.ToList())[1].Split(',');
                                    Recordset oRcs = null;

                                    #region Labels ALLIX
                                    SAPbobsCOM.GeneralService oGeneralService;
                                    SAPbobsCOM.GeneralData oGeneralData;

                                    SAPbobsCOM.GeneralDataCollection oSons;
                                    SAPbobsCOM.GeneralData oSon;

                                    SAPbobsCOM.CompanyService sCmp = oCompany.GetCompanyService();

                                    oGeneralService = sCmp.GetGeneralService("LABELLING");
                                    oGeneralData = (SAPbobsCOM.GeneralData)oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralData);
                                    oGeneralData.SetProperty("Code", CodeDet[2].Trim() + "." + Formula[18].Trim() + ".FR");
                                    oGeneralData.SetProperty("U_INVFormulaCode", CodeDet[2].Trim());
                                    oGeneralData.SetProperty("U_INVCodeLang", "FR");
                                    oGeneralData.SetProperty("U_INVOrderNum", Formula[18].Trim());
                                    oGeneralData.SetProperty("Name", CodeDet[2].Trim() + "." + Formula[18].Trim() + ".FR");

                                    string RubricCode = string.Empty;
                                    string OldRubricCode = string.Empty;
                                    int HRubricLine = 0;



                                    int rubricline = 0;
                                    foreach (var linie in labelLines)
                                    {
                                        string linie2 = linie.Replace("," + CodeDet[2], "");
                                        string info = linie2.Substring(4, linie2.LastIndexOf(',') - 4);
                                        RubricCode = linie2.Substring(2, 1);
                                        if (RubricCode != "-" && RubricCode != "0")
                                        {
                                            if (string.IsNullOrEmpty(OldRubricCode))
                                            {
                                                OldRubricCode = RubricCode;
                                            }

                                            if (OldRubricCode != RubricCode)
                                            {
                                                rubricline = 0;
                                                OldRubricCode = RubricCode;
                                            }
                                            if (RubricCode == "H")
                                            {
                                                HRubricLine++;
                                                rubricline = HRubricLine;
                                            }
                                            if (OldRubricCode == "I" && RubricCode == "I" && rubricline < 100)
                                            {
                                                rubricline = HRubricLine * 100;
                                            }
                                            oSons = oGeneralData.Child("INVLABELLINGL");
                                            oSon = oSons.Add();
                                            oSon.SetProperty("U_INVRubricCode", RubricCode);
                                            oSon.SetProperty("U_INVRubricLine", rubricline);
                                            oSon.SetProperty("U_INVRubricVal", info);

                                            rubricline++;
                                        }


                                    }
                                    try
                                    {
                                        oGeneralService.Add(oGeneralData);
                                    }
                                    catch (Exception ex)
                                    {
                                        HasErrors = true;
                                        WriteErrorTable(ex.Message + oCompany.GetLastErrorDescription() + "-Label-"+ CodeDet[2].Trim() + " with Revision:" + Formula[18].Trim(), file, "IA");
                                       // Write_To_file(ex.Message + " label " + oCompany.GetLastErrorDescription());
                                    }

                                    #endregion

                                    #region ItemDetails 
                                    IItemDetails itemDetails = PFcompany.CreatePFObject(ObjectTypes.ItemDetails);
                                    oRcs = (Recordset)oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

                                    oRcs.DoQuery(string.Format("select Code from [@CT_PF_OIDT] where U_ItemCode='{0}'", CodeDet[2].Trim()));

                                    if (oRcs.RecordCount > 0)
                                    {

                                        string key = oRcs.Fields.Item("Code").Value.ToString().Trim();
                                        oRcs.MoveFirst();
                                        itemDetails.GetByKey(key);
                                        itemDetails.Revisions.SetCurrentLine(itemDetails.Revisions.Count - 1);

                                        itemDetails.Revisions.U_Code = Formula[18].Trim();
                                        itemDetails.Revisions.U_Description = Formula[18].Trim();
                                        itemDetails.Revisions.U_ValidFrom = Convert.ToDateTime(DateTime.Now.ToShortDateString());
                                        //itemDetails.Revisions.U_ValidTo = DateTime.Now;
                                        itemDetails.Revisions.U_Status = RevisionStatus.Engineering;
                                        itemDetails.Revisions.U_Default = CompuTec.ProcessForce.API.Enumerators.YesNoType.No;
                                        itemDetails.Revisions.U_IsMrpDefault = CompuTec.ProcessForce.API.Enumerators.YesNoType.No;
                                        itemDetails.Revisions.UDFItems.Item("U_INV_FormulaID").Value = Formula[18].Trim();
                                        itemDetails.Revisions.UDFItems.Item("U_INVLabelling").Value = CodeDet[2].Trim() + "." + Formula[18].Trim() + ".FR";
                                        itemDetails.Revisions.U_IsCostingDefault = CompuTec.ProcessForce.API.Enumerators.YesNoType.No;
                                        //itemDetails.Revisions.U_Remarks = "test";
                                        //itemDetails.Revisions.Add();

                                        AddID = 0;
                                        try
                                        {
                                            AddID = itemDetails.Update();
                                        }
                                        catch (Exception ex)
                                        {
                                            WriteErrorTable(ex.Message + "-Item Details-" + CodeDet[2].Trim() + " with Revision:" + Formula[18].Trim(), file, "IA");
                                           // Write_To_file(ex.Message + " ItemDetails");
                                            HasErrors = true;
                                        }
                                        if (AddID != 0)
                                        {
                                            WriteErrorTable(PFcompany.getLastErrorDescription() + "-Item Details-" + CodeDet[2].Trim() + " with Revision:" + Formula[18].Trim(), file, "IA");
                                           // Write_To_file(PFcompany.getLastErrorDescription());
                                            HasErrors = true;

                                        }
                                        else
                                        {
                                            Write_To_file("Succesfull added ItemDetails  for ItemCode:" + CodeDet[2].Trim() + " with Revision:" + Formula[18].Trim());
                                        }
                                    }
                                    #endregion

                                    #region BOM
                                    IBillOfMaterial billOfMaterial = PFcompany.CreatePFObject(ObjectTypes.BillOfMaterial);
                                    billOfMaterial.U_ItemCode = CodeDet[2].Trim();

                                    billOfMaterial.U_Factor = 1;
                                    billOfMaterial.U_Quantity = 1;
                                    billOfMaterial.U_Revision = Formula[18].Trim();
                                    billOfMaterial.U_ProdType = CompuTec.ProcessForce.API.Enumerators.ProductionType.Internal;
                                    billOfMaterial.U_InventoryUoM = "KG";
                                    billOfMaterial.U_BatchSize = 1;
                                    billOfMaterial.U_WhsCode = "GA5" + CodeDet[4].Trim() + "CON";
                                    oRcs = (Recordset)oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                                    foreach (var lin in Item_LInes)
                                    {
                                        //string linie=lin.Replace("1,0,4000.16   ,", "");
                                        string[] linie = lin.Split(',');
                                        oRcs.DoQuery(string.Format("select top 1 ItemCode,ItemName from OITM where U_INVCodeFormula='{0}'", linie[3].Trim()));
                                        oRcs.MoveFirst();
                                        billOfMaterial.Items.U_ItemCode = oRcs.Fields.Item("ItemCode").Value.ToString().Trim();
                                        billOfMaterial.Items.U_Quantity = double.Parse(linie[9].Trim());
                                        billOfMaterial.Items.U_Factor = 100;
                                        billOfMaterial.Items.U_Yield = 100;
                                        billOfMaterial.Items.U_IssueType = "B";
                                        billOfMaterial.Items.U_InventoryUoM = "KG";
                                        billOfMaterial.Items.U_WhsCode= "GA5" + CodeDet[4].Trim() + "FAB";
                                        billOfMaterial.Items.Add();

                                    }
                                    oRcs.DoQuery(string.Format(@"exec Return_Resource '{0}'", CodeDet[2].Trim()));
                                    if (oRcs.RecordCount > 0)
                                    {
                                        billOfMaterial.Items.U_ItemCode = oRcs.Fields.Item("ItemCode").Value.ToString().Trim();
                                        billOfMaterial.Items.U_Quantity = 1;
                                        billOfMaterial.Items.U_Factor = 100;
                                        billOfMaterial.Items.U_Yield = 100;
                                        billOfMaterial.Items.U_IssueType = "B";
                                        billOfMaterial.Items.U_InventoryUoM = "KG";
                                        billOfMaterial.Items.U_WhsCode = "GA5" + CodeDet[4].Trim() + "FAB";
                                        billOfMaterial.Items.Add();
                                    }




                                    AddBom = 0;
                                    try
                                    {

                                        AddBom = billOfMaterial.Add();
                                    }
                                    catch (Exception ex)
                                    {
                                        WriteErrorTable(PFcompany.getLastErrorDescription() + "-BOM-" + CodeDet[2].Trim() + " with Revision:" + Formula[18].Trim(), file, "IA");
                                        HasErrors = true;
                                       // Write_To_file(ex.Message + " BOM");
                                    }
                                    if (AddBom != 0)
                                    {
                                       // Write_To_file(PFcompany.getLastErrorDescription());

                                    }
                                    else
                                    {
                                        Write_To_file("Succesfull added BOM for ItemCode:" + CodeDet[2].Trim() + "with Revision:" + Formula[18].Trim());
                                    }


                                    #endregion

                                    #endregion


                                }
                               /// Write_To_file("PathFail= "+PathFail.ToString());
                             ///   Write_To_file("HasErrors= " + HasErrors.ToString());

                                if (!HasErrors)
                                {
                                    File.Move(file, PathSucces + filename);
                                }
                                else
                                {
                                    try
                                    {
                                        File.Move(file, PathFail + filename);
                                    }
                                    catch(Exception ex)
                                    {
                                        Write_To_file(ex.Message);
                                    }
                                }

                            }

                        }
                        #endregion
                    }
                    if (ConfigurationManager.AppSettings["Import"] == "1")
                    {
                        #region Import

                        if (PFcompany.IsConnected)
                        {
                            string filename = string.Empty;
                            string DocEntryNumber = string.Empty;
                            string PathImport = ConfigurationManager.AppSettings["ImportPath"];
                            string PathFail = ConfigurationManager.AppSettings["ImportPath"] + "KO\\";
                            if (!Directory.Exists(PathFail))
                            {
                                Directory.CreateDirectory(PathFail);
                            }
                            string PathSucces = ConfigurationManager.AppSettings["ImportPath"] + "OK\\";
                            if (!Directory.Exists(PathSucces))
                            {
                                Directory.CreateDirectory(PathSucces);
                            }
                            string[] files = Directory.GetFiles(PathImport);
                            Write_To_file("S-au gasit " + files.Count().ToString() + " fisiere consum de procesat");
                            foreach (string file in files)
                            {

                                try
                                {
                                    filename = Path.GetFileName(file);
                                    Write_To_file("Se proceseaza fisierul " + filename);

                                    //PFcompany.StartTransaction();
                                    XmlDocument xmlDoc = new XmlDocument(); // Create an XML document object
                                    xmlDoc.Load(file);
                                    XmlNode Root = xmlDoc.DocumentElement;

                                    string Site = Root.ChildNodes[0].InnerText.ToString();
                                    string WONumber = Root.ChildNodes[1].InnerText.ToString();
                                    DocEntryNumber = Root.ChildNodes[2].InnerText.ToString();
                                    string ItemCode = Root.ChildNodes[3].InnerText.ToString();
                                    string ClosingFlag = Root.ChildNodes[4].InnerText.ToString();
                                    int PickOrderDocEntry = 0;
                                    Write_To_file("Geting components");
                                    XmlNodeList Components = xmlDoc.GetElementsByTagName("Component");
                                    Write_To_file("Have components");
                                    List<XmlNode> nodes = Components.Cast<XmlNode>().ToList();
                                    ICreatePickOrderForProductionIssue prAction = PFcompany.CreatePFAction(CompuTec.ProcessForce.API.Core.ActionType.CreatePickOrderForProductionIssue);

                                    prAction.AddMORDocEntry(int.Parse(DocEntryNumber));
                                    prAction.Return_type = OutputType.PickOrder;
                                    object PicOrderDocEntry;
                                    if (prAction.DoAction(out PicOrderDocEntry))
                                    {
                                        PickOrderDocEntry = int.Parse(PicOrderDocEntry.ToString());

                                    }
                                    IPickOrder oPickOrder_CP = PFcompany.CreatePFObject(CompuTec.ProcessForce.API.Core.ObjectTypes.PickOrder);
                                    oPickOrder_CP.GetByKey(PickOrderDocEntry.ToString());
                                    int Uom = 1;
                                    for (int idx = 0; idx < oPickOrder_CP.RequiredItems.Count; idx++)
                                    {

                                        oPickOrder_CP.RequiredItems.SetCurrentLine(idx);
                                        if (oPickOrder_CP.RequiredItems.DS_ItemUoM == "KG")
                                            Uom = 1000;
                                        else
                                            Uom = 1;

                                        string itemcode = oPickOrder_CP.RequiredItems.U_ItemCode;
                                        if (!string.IsNullOrEmpty(itemcode))
                                        {
                                            Write_To_file("ItemCode components "+itemcode);
                                            XmlNode nod = nodes.First(p => p.Attributes[0].Value == itemcode);
                                            XmlNode node = nod.LastChild;
                                            Write_To_file("Have Childs");
                                            oPickOrder_CP.RequiredItems.SetCurrentLine(idx);
                                            oPickOrder_CP.RequiredItems.U_PickedQty = double.Parse(node.ChildNodes[1].InnerText) / Uom;
                                            var ReqItemsLineNum = oPickOrder_CP.RequiredItems.U_LineNum;
                                            oPickOrder_CP.PickedItems.SetCurrentLine(idx);
                                            oPickOrder_CP.PickedItems.U_ItemCode = oPickOrder_CP.RequiredItems.U_ItemCode;
                                            oPickOrder_CP.PickedItems.U_BnDistNumber = node.ChildNodes[0].InnerText;
                                            oPickOrder_CP.PickedItems.U_Quantity = double.Parse(node.ChildNodes[1].InnerText) / Uom;
                                            var PickedItemsLineNum = oPickOrder_CP.PickedItems.U_LineNum;
                                            oPickOrder_CP.Relations.U_PickItemLineNo = PickedItemsLineNum;
                                            oPickOrder_CP.Relations.U_ReqItemLineNo = ReqItemsLineNum;
                                            oPickOrder_CP.Relations.Add();
                                            oPickOrder_CP.PickedItems.Add();
                                        }
                                    }
                                    int retc_CP = oPickOrder_CP.Update();
                                    if (retc_CP == 0)
                                    {
                                        Write_To_file("Added pickIssue for MOR " + DocEntryNumber);
                                        CompuTec.ProcessForce.API.Actions.CreateGoodsIssueFromPickOrderBasedOnProductionIssue.ICreateGoodsIssueFromPickOrderBasedOnProductionIssue action = PFcompany.CreatePFAction(ActionType.CreateGoodsIssueFromPickOrderBasedOnProductionIssue);
                                        action.PickOrderID = oPickOrder_CP.DocEntry;
                                        action.DocDate = DateTime.Today;
                                        action.TaxDate = DateTime.Today;
                                        object GoodsIssueEntry = 0;
                                        string GoodsIssueDocEntry = "";
                                        if (action.DoAction(out GoodsIssueEntry))
                                        {
                                            GoodsIssueDocEntry = GoodsIssueEntry.ToString();

                                        }
                                        if (!string.IsNullOrEmpty(GoodsIssueDocEntry))
                                        {
                                            Write_To_file("Am  adaugat cu succes issue from production pentru mor " + DocEntryNumber + " File " + filename);
                                            //if (PFcompany.InTransaction)
                                            //    PFcompany.EndTransaction(StopTransactionType.Commit );
                                            File.Move(file, PathSucces + filename);
                                        }
                                        else
                                        {
                                            WriteErrorTable("Failed to add issue from production for mor "+PFcompany.getLastErrorDescription() + "-ISSUE-" + DocEntryNumber+" with Item code"+ ItemCode, filename, "IR");
                                            Write_To_file("Failed to add issue from production for mor " + DocEntryNumber + " File " + filename);

                                            //if (PFcompany.InTransaction)
                                            //    PFcompany.EndTransaction(StopTransactionType.Rollback);
                                            File.Move(file, PathFail + filename);
                                        }

                                    }
                                    else
                                    {
                                        WriteErrorTable("Error adding pickIssue for MOR " + PFcompany.getLastErrorDescription() + "-PICK ISSUE-" + DocEntryNumber + " with Item code" + ItemCode, filename, "IR");
                                        Write_To_file("Error adding pickIssue for MOR " + DocEntryNumber + " File " + filename);
                                        //if (PFcompany.InTransaction)
                                        //    PFcompany.EndTransaction(StopTransactionType.Rollback);
                                        File.Move(file, PathFail + filename);
                                    }

                                }
                                catch (Exception Ex)
                                {
                                    WriteErrorTable("Error adding pickIssue for MOR " + PFcompany.getLastErrorDescription().Replace("'","") + Ex.Message.Replace("'","") + "-ISSUE-" + DocEntryNumber , filename, "IR");
                                    Write_To_file("Error adding pickIssue for MOR " + DocEntryNumber + " File " + filename + " with error :" + Ex.Message + " " + Ex.InnerException);
                                    //if (PFcompany.InTransaction)
                                    //    PFcompany.EndTransaction(StopTransactionType.Rollback);
                                    File.Move(file, PathFail + filename);
                                }


                            }                          
                        }
                        #endregion
                    }
                    if (ConfigurationManager.AppSettings["Export"] == "1")
                    {
                        #region Export
                        SAPbobsCOM.Recordset oRcsUpdate = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                        SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                        SAPbobsCOM.Recordset oRecordset2 = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                        SAPbobsCOM.Recordset oRecordset3 = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                        string QueryHeader = @"exec HeaderExport";

                        oRecordset.DoQuery(QueryHeader);
                        if (oRecordset.RecordCount > 0)
                        {
                            for (oRecordset.MoveFirst(); !oRecordset.EoF; oRecordset.MoveNext())
                            {
                                int i;
                                XmlDocument doc = new XmlDocument();
                                XmlNode newnode = doc.CreateXmlDeclaration("1.0", "UTF-8", "");
                                XmlAttribute newatrib;
                                XmlElement newelement;
                                doc.AppendChild(newnode);
                                newnode = doc.CreateNode(XmlNodeType.Element, "ProductionOrder", "");
                                doc.AppendChild(newnode);
                                XmlElement parentEle = doc.DocumentElement;
                                string val;
                                string DocEntry = oRecordset.Fields.Item("WONumber").Value.ToString().Trim();
                                string site = oRecordset.Fields.Item("Site").Value.ToString().Trim();
                                #region Header
                                for (i = 0; i < oRecordset.Fields.Count; i++)
                                {
                                    newnode = doc.CreateNode(XmlNodeType.Element, oRecordset.Fields.Item(i).Name, "");

                                    val = oRecordset.Fields.Item(i).Value.ToString().Trim();
                                    if (!string.IsNullOrEmpty(val))
                                        newnode.InnerText = val;
                                    parentEle.AppendChild(newnode);
                                }
                                #endregion
                                #region Component
                                oRecordset2.DoQuery(string.Format("exec ItemsExport {0}", DocEntry));
                                if (oRecordset2.RecordCount > 0)
                                {
                                    for (oRecordset2.MoveFirst(); !oRecordset2.EoF; oRecordset2.MoveNext())
                                    {
                                        val = oRecordset2.Fields.Item(0).Value.ToString().Trim();

                                        newelement = doc.CreateElement("Component");
                                        newelement.SetAttribute("ID", val);

                                        for (i = 1; i < oRecordset2.Fields.Count; i++)
                                        {
                                            newnode = doc.CreateNode(XmlNodeType.Element, oRecordset2.Fields.Item(i).Name, "");
                                            val = oRecordset2.Fields.Item(i).Value.ToString().Trim();
                                            if (!string.IsNullOrEmpty(val))
                                                newnode.InnerText = val;
                                            newelement.AppendChild(newnode);
                                        }
                                        //newnode = doc.CreateNode(XmlNodeType.Element, "ByProduct", "");
                                        //XmlNode ch = doc.CreateNode(XmlNodeType.Element, "ByProductCode", "");
                                        //newnode.AppendChild(ch);
                                        //newelement.AppendChild(newnode);
                                        parentEle.AppendChild(newelement);


                                    }
                                }
                                #endregion
                                #region Operations
                                oRecordset3.DoQuery(string.Format("exec OperationsExport {0}", DocEntry));
                                if (oRecordset3.RecordCount > 0)
                                {
                                    for (oRecordset3.MoveFirst(); !oRecordset3.EoF; oRecordset3.MoveNext())
                                    {
                                        val = oRecordset3.Fields.Item(0).Value.ToString().Trim();
                                        newelement = doc.CreateElement("Operation");
                                        newelement.SetAttribute("ID", val);
                                        for (i = 1; i < oRecordset3.Fields.Count; i++)
                                        {
                                            newnode = doc.CreateNode(XmlNodeType.Element, oRecordset3.Fields.Item(i).Name, "");
                                            val = oRecordset3.Fields.Item(i).Value.ToString().Trim();
                                            if (!string.IsNullOrEmpty(val))
                                                newnode.InnerText = val;
                                            newelement.AppendChild(newnode);
                                        }
                                        newnode = doc.CreateNode(XmlNodeType.Element, "ByProduct", "");
                                        XmlNode ch = doc.CreateNode(XmlNodeType.Element, "ByProductCode", "");
                                        newnode.AppendChild(ch);
                                        newelement.AppendChild(newnode);
                                        parentEle.AppendChild(newelement);
                                    }
                                }
                                #endregion
                                #region Label   
                                XmlElement Label = doc.CreateElement("Label");
                                XmlElement LabelSections = doc.CreateElement("LabelSections");
                                XmlElement LabelSectionType = doc.CreateElement("LabelSectionType");
                                XmlElement LabelSectionsLines = doc.CreateElement("LabelSectionLines");
                                XmlElement LabelLinenumber = doc.CreateElement("LabelLinenumber");
                                XmlElement LabelLineDescription = doc.CreateElement("LabelLineDescription");
                                LabelSectionsLines.AppendChild(LabelLinenumber);
                                LabelSectionsLines.AppendChild(LabelLineDescription);
                                LabelSections.AppendChild(LabelSectionType);
                                LabelSections.AppendChild(LabelSectionsLines);
                                Label.AppendChild(LabelSections);
                                parentEle.AppendChild(Label);
                                #endregion
                                try
                                {
                                    doc.Save(ConfigurationManager.AppSettings["SavePath"] + site + "_OX" + DocEntry + ".xml");
                                    if (ConfigurationManager.AppSettings["Dev"] != "Y")
                                    {
                                        oRcsUpdate.DoQuery(string.Format(@"update [@CT_PF_OMOR] set U_INVMOStatus='SEN' where DocEntry='{0}'", DocEntry));
                                        Write_To_file("Updated document with entry " + DocEntry);
                                    }
                                    Write_To_file("Status= DEV document with entry " + DocEntry + "won't be updated");
                                }
                                catch (Exception ex)
                                {
                                    WriteErrorTable("Error exporting MOR " + PFcompany.getLastErrorDescription() + ex.Message + "-MOR-" + DocEntry, DocEntry+".xml", "EM");
                                    Write_To_file(ex.Message);
                                }

                            }
                        }
                        #endregion
                    }
                    Write_To_file(" END waiting " + (SleepTime * 60).ToString() + " seconds");
                   System.Threading.Thread.Sleep(SleepTime * 60 * 1000);
                }
                PFcompany.Disconnect();
                oCompany.Disconnect();
            }
            else
            {
                WriteErrorTable("Error connecting to database " + PFcompany.getLastErrorDescription() + oCompany.GetLastErrorDescription() + "-CONNECT-"," ", "EM");
                Write_To_file("There was a problem connecting to the database!");
            }
        }
    }
}
